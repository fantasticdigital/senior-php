const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('wp-content/themes/custom-theme/assets/js/app.js', 'wp-content/themes/custom-theme/dist/js/').sass('wp-content/themes/custom-theme/assets/scss/app.scss', 'wp-content/themes/custom-theme/dist/css').options({ processCssUrls: false });
mix.js('wp-content/plugins/custom-block/assets/js/app.js', 'wp-content/plugins/custom-block/dist/js/').sass('wp-content/plugins/custom-block/assets/scss/app.scss', 'wp-content/plugins/custom-block/dist/css').options({ processCssUrls: false });
