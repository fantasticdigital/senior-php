<?php  

if ( !function_exists( 'add_theme_support' ) ) {

	add_action( 'after_setup_theme', 'add_theme_support' );

	function add_theme_support()
	{
		// Include any additional support options for the custom theme
	}
}

if ( !function_exists( 'add_styles' ) ) {

	add_action( 'wp_enqueue_scripts', 'add_styles' );
	
	function add_styles()
	{
		if ( file_exists( get_stylesheet_directory() . '/dist/css/app.css' ) ) {
			wp_enqueue_style( 'custom-theme-app', get_template_directory_uri() . '/dist/css/app.css', array(), filemtime( get_stylesheet_directory() . '/dist/css/app.css' ) );
		}

		// Include additional stylesheets
	}
}

if ( !function_exists( 'add_scripts' ) ) {

	add_action( 'wp_enqueue_scripts', 'add_scripts' );
	
	function add_scripts()
	{
		if ( file_exists( get_stylesheet_directory() . '/dist/js/app.js' ) ) {
			wp_enqueue_script( 'custom-theme-app', get_template_directory_uri() . '/dist/js/app.js', array( 'jquery' ), filemtime( get_stylesheet_directory() . '/dist/js/app.js' ), true );
		}
		
		// Include additional script files
	}
}