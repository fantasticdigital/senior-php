<?php
/**
 * @package Custom Block
 * @version 1.0.0
 * 
 * Plugin Name:     Custom Block
 * Plugin URI:      https://www.fantasticmedia.co.uk/
 * Description:     
 * Author:          Fantastic Media
 * Author URI:      https://www.fantasticmedia.co.uk/
 * Version:         1.0.0
 * Author URI:      https://www.fantasticmedia.co.uk/
 * License:         GPL v2 or later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
    echo esc_attr( 'Hi there!  I\'m just a plugin, not much I can do when called directly.' );
    exit;
}

// Define globals properties
define( 'CB__VERSION', '1.0.0' );
define( 'CB__NAME', 'Custom Block' );
define( 'CB__MINIMUM_WP_VERSION', '5.8' );
define( 'CB__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'CB__PLUGIN_URI', plugin_dir_url( __FILE__ ) );

// Setup functions
if ( !function_exists( 'cb_add_styles' ) ) {

    add_action( 'wp_enqueue_scripts', 'cb_add_styles' );
    
    function cb_add_styles()
    {
        // Include front end styles
    }
}

if ( !function_exists( 'cb_add_admin_styles' ) ) {

    add_action( 'admin_enqueue_scripts', 'cb_add_admin_styles' );
    
    function cb_add_admin_styles()
    {
        if ( file_exists( CB__PLUGIN_DIR . 'dist/css/app.css' ) ) {
            wp_enqueue_style( 'custom-block-admin', CB__PLUGIN_URI . 'dist/css/app.css', array(), filemtime( CB__PLUGIN_DIR . 'dist/css/app.css' ) );
        }

        // Include additional admin stylesheets
    }
}

if ( !function_exists( 'cb_add_scripts' ) ) {

    add_action( 'wp_enqueue_scripts', 'cb_add_scripts' );
    
    function cb_add_scripts()
    {
        // Include front end script files
    }
}

if ( !function_exists( 'cb_add_admin_scripts' ) ) {

    add_action( 'admin_enqueue_scripts', 'cb_add_admin_scripts' );
    
    function cb_add_admin_scripts()
    {
        if ( file_exists( CB__PLUGIN_DIR . 'dist/js/app.js' ) ) {
            wp_enqueue_script( 'custom-block-admin', CB__PLUGIN_URI . 'dist/js/app.js', array( 'jquery' ), filemtime( CB__PLUGIN_DIR . 'dist/js/app.js' ), true );
        }

        // Include additional admin script files
    }
}