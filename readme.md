# Technical Test

Welcome to the Fantastic Media technical test for Senior PHP Developers. This will test your knowledge of WordPress. Working with themes and plugins as well as creating custom Gutenberg blocks. This test should take between 2 and 5 hours to complete in full.

## Prerequisites

We have created a folder structure along with all of the basic files you will need to complete these tasks. The structure is split in to two areas, the plugin and theme.

`wp-content/plugins/custom-block` holds all of the files that will be used within the custom block plugin. We have created an `index.php` file to include the relevant styles and scripts as well as make the plugin available to be activated via the WordPress admin.

`wp-content/themes/custom-theme` holds all of the required files for the theme and building your custom web pages. This includes both scripts and stylesheets that have both been included in the front end pages via the `functions.php` file. We have also created a custom page template `wp-content/themes/custom-theme/custom-page.php` which can be selected via the CMS.

# Getting Started

## Install WordPress

- Clone the repository `git clone git@bitbucket.org:fantasticdigital/senior-php.git`
- Setup locally and run the WordPress install
- Change the active theme to **Custom Theme**
- Activate the **Custom Block** plugin
- Create a new page called **Home**
- Create new page called **Custom Page** and set the page template to **Custom Page**

## Setup Syles and Script

As mentioned above we have created script and stylesheets for both the plugin and theme. These need to be compiled in order for them to be used on the front end of the site. To do this we have included webpack which will automatically compile both your JS and SCSS whenever you make a change to them. In order for this to work you will need to do the following steps:

- Run `npm install` in your command line to download all of the dependencies
- Each time you start working on the project run `npm run watch` (this will listen for any changes you make to the scripts and styles then automatically compile them)

# The Brief

## Part 1: Design to HTML (est. 2 hours)
Using the newly created Custom Page, use the `custom-theme/custom-page.php` file to build the web page design (the design and required assets can be found here `/files/web.png`). 

All text and images should be editable via the CMS (feel free to use a third party plugin such as ACF for this). Do not use any plugins for the layout and styling, all of this should be done by you using HTML, CSS and JS. Bootstrap has been included in the custom theme and your layout should utilise the Bootstrap grid system. 

Your finished web page should work seamlessly across all devices and should load lightning fast (think about using lazy loading and other techniques to speed up the load time). We have only provided a desktop design so use your best judgement to make the page look good on tablet and mobile devices.

## Prerequisites

- Stylesheet has been created and included on the site which includes Bootstrap `custom-theme/assets/scss/app.scss`
- You can override Bootstrap variables using `custom-theme/assets/scss/_variables.scss`
- Script file has been created and included on the site `custom-theme/assets/js/app.js`. All of your custom js should go in this file.
- See *Setup Styles and Script* details under *Getting Started* to understand how this all functions
- Heading font: https://fonts.google.com/specimen/DM+Serif+Text
- Body font: https://fonts.google.com/specimen/Montserrat
- Icon font: http://zavoloklom.github.io/material-design-iconic-font/index.html
- Content is saved in `/files/web-content.txt`

### You will be scored on the following...

- Text and imagery should be editable via the CMS
- Bootstrap should be utilised for the layout
- Should work seamlessly across devices
- Web page should match the designs (think about spacing and font sizes etc)

### You can earn bonus points by...

- Using techniques to reduce load time
- Including transitions and / or parallax etc 
- Commenting your code
- Completing the task in under 2 hours

## Part 2: Create a Custom Block (est. 2 hours)
Using the custom plugin that has been setup for you, you will need to create a custom block that allows the user to add a Bootstrap alert component to a page. The custom block should include the ability to choose between Bootstrap options which will be available in the block settings in the right sidebar. 

These will include: **Contextual classes** (.alert-primary, .alert-secondary, .alert-success, .alert-danger, .alert-warning and .alert-info ) and the ability to make the alert **dismisable** (this should be a toggle option). 

Within the block editor the user should be able to see a live preview of the alert component along with being able to add inner content to the block itself. As options are toggled in the right sidebar, the preview in the block editor should update. 

When this custom block is added to a page it should then be rendered using php on the front end. You can test your block on the homepage that was created. There is a short video to show how this should work here `/files/alert.webm`. We have included a script and stylesheet within the admin `custom-block/assets/*` where you can put all of your custom code.

### You will be scored on the following...

- A new custom block that can be added in the block editor
- Allow user to choose between contextual classes
- Allow user to select dismissable (this should add a close button to the block and when clicked the alert should dissapear)
- Show a live preview of th block as options are updated
- Render the block using PHP on the front end of the site

### You can earn bonus points by...

- Allow the user to add an icon to the alert
- Allow th user to click a button to display an alert
- Commenting your code
- Completing the task in under 2 hours

### Useful links

- Bootstrap alert component (contextual classes): https://getbootstrap.com/docs/5.1/components/alerts/#examples
- Bootstrap alert component (dismissing): https://getbootstrap.com/docs/5.1/components/alerts/#dismissing
- WordPress custom blocks docs: https://developer.wordpress.org/block-editor/how-to-guides/block-tutorial/writing-your-first-block-type/

## Part 3: Zip and Send (est. 30 mins)
Once you have completed the above tasks you need to package up your site and send it across to us for review. Make sure you include an export of your full database, all of the WordPress files along with any instructions we need to follow in order to get it up and running for testing.

### You will be scored on the following...

- Includes full export of database
- Includes all site files
- Includes any required instructions for setup
